/******************************************************************************
*
* libkuso
* Disables libc memory functions
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef LIBKUSO_MEM_DISABLE_H_
#define LIBKUSO_MEM_DISABLE_H_

#undef malloc
#undef calloc
#undef realloc
#undef free
#undef strdup

#define malloc(x)    STATIC_ASSERT_NULL(0, malloc_disabled__use_kuso_memory_functions)
#define calloc(x,y)  STATIC_ASSERT_NULL(0, calloc_disabled__use_kuso_memory_functions)
#define realloc(x,y) STATIC_ASSERT_NULL(0, realloc_disabled__use_kuso_memory_functions)
#define free(x)      STATIC_ASSERT(0, free_disabled__use_kuso_memory_functions)
#define strdup(x)    STATIC_ASSERT_NULL(0, strdup_disabled__use_kuso_memory_functions)

void* _yes_I_really_know_what_Im_doing_malloc(size_t size);
void* _yes_I_really_know_what_Im_doing_calloc(size_t cnt, size_t size);
void* _yes_I_really_know_what_Im_doing_realloc(void* ptr, size_t size);
void _yes_I_really_know_what_Im_doing_free(void* ptr);
char* _yes_I_really_know_what_Im_doing_strdup(char* str);

#endif
