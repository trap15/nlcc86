/******************************************************************************
*
* libkuso
* Assert support header
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*******************************************************************************
*
* To do a static assertion: (errors during compilation)
*     STATIC_ASSERT(cond, message);
* Where cond is the condition to NOT throw the assert.
* message must be all lowercase, and a valid symbol.
*   A good message scheme is generic_error__specific_error
*
* To do a runtime assertions: (errors while running)
*     ASSERT(cond, message);
* Where cond is the condition to NOT throw the assert.
* message is a C string.
*
******************************************************************************/

#ifndef LIBKUSO_ASSERT_H_
#define LIBKUSO_ASSERT_H_

/* Static assert */
#define STATIC_ASSERT(expr, msg) \
extern struct { int STATIC_ASSERT__##msg:(expr)?1:-1; } CPP_EVAL3(STATIC_ASSERT__##msg, _, __LINE__)

#define STATIC_ASSERT_ZERO(expr, msg) \
(sizeof(struct { int STATIC_ASSERT__##msg:(expr)?1:-1; }) ? 0 : 0)
#define STATIC_ASSERT_NULL(expr, msg) \
((void*)STATIC_ASSERT_ZERO(expr, msg))

/* Run-time assert */
#define ASSERT(cond, msg) do { \
  if(!(cond)) { \
    fprintf(stderr, "Assert " #cond " (%s) failure! Line %d, file %s\n", msg, __LINE__, __FILE__); \
    __builtin_trap(); \
    exit(EXIT_FAILURE); \
  } \
} while(0)

#endif
