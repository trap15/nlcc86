/******************************************************************************
*
* libkuso
* Miscellaneous support header
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*******************************************************************************
*
* Use type 'int' for boolean values, but you may use TRUE and FALSE.
*
******************************************************************************/

#ifndef LIBKUSO_MISC_H_
#define LIBKUSO_MISC_H_

/* Boolean */
#define TRUE  1
#define FALSE 0

#define CPP_EVAL1(x) x
#define CPP_EVAL2(x,y) x##y
#define CPP_EVAL3(x,y,z) x##y##z
#define CPP_EVAL4(x,y,z,w) x##y##z##w
#define CPP_CONCAT2(x,y) CPP_EVAL2(x,y)
#define CPP_CONCAT3(x,y,z) CPP_EVAL3(x,y,z)
#define CPP_CONCAT4(x,y,z,w) CPP_EVAL4(x,y,z,w)

#endif
