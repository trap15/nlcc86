#include "top.h"

const char *g_app;
const char *g_in_file = NULL;
const char *g_out_file = NULL;
const char *g_lsic86 = "";
const char *g_nc86opt = "";
int g_verbosity = 0;
int g_dryrun = 0;
int g_out_type = OUTPUT_TYPE_EXE;
int g_use_dosexec = 0;

CPPState g_cpp;

static int prv_process_option(char **arg) {
  switch (arg[0][1]) {
    case '~': // Use dosexec
      g_use_dosexec = 1;
      return 1;
    case 'I': // Include directory
      if (arg[0][2] == '\0') { // directory name in separate argument
        kuso_pvec_append(g_cpp.include_dirs, arg[1]);
        return 2;
      }
      kuso_pvec_append(g_cpp.include_dirs, arg[0]+2);
      return 1;
    case 'D': // Define
      kuso_pvec_append(g_cpp.defines, arg[0]+2);
      return 1;
    case 'X': // LSI C-86 base directory
      if (g_lsic86[0] != '\0') {
        fprintf(stderr, "Warning: overwriting LSI C-86 basedir\n");
      }
      g_lsic86 = arg[0]+2;
      return 1;
    case 'q': // nc86opt directory
      if (g_nc86opt[0] != '\0') {
        fprintf(stderr, "Warning: overwriting NC86OPT basedir\n");
      }
      g_nc86opt = arg[0]+2;
      return 1;
    case 'v': // Verbosity level
      g_verbosity = xtoi(arg[0]+2);
      return 1;
    case 'z': // dry run
      g_dryrun = 1;
      return 1;
    case 'c': // output type: object
      g_out_type = OUTPUT_TYPE_OBJECT;
      return 1;
    case 'S': // output type: assembler
      g_out_type = OUTPUT_TYPE_ASM;
      if (arg[0][2] == 'C') { // With C
        g_out_type = OUTPUT_TYPE_ASM_WITH_C;
      }
      return 1;
    case 'o': // output file
      if (g_out_file) {
        fprintf(stderr, "Warning: overwriting output filename\n");
      }
      if (arg[0][2] == '\0') { // file name in separate argument
        g_out_file = arg[1];
        return 2;
      }
      g_out_file = arg[0]+2;
      return 1;
    default:
      fprintf(stderr, "Error: Unknown option '%s'\n", arg[0]);
      return 0;
  }
}

static int prv_process_argument(char **arg) {
  switch (arg[0][0]) {
    case '@': // load options from file
      fprintf(stderr, "File-based arguments not yet supported!\n");
      return 1;
    case '-': // option
      return prv_process_option(arg);
    default: // input file
      if (g_in_file) {
        fprintf(stderr, "Compiling multiple files at once not yet supported!\n");
        return 0;
      }
      g_in_file = arg[0];
      return 1;
  }
  return 0;
}

#define TMPFN_LEN 11
enum {
  TMPFN_CPP,
  TMPFN_CF,
  TMPFN_ASM,
  TMPFN_MIXED,
  TMPFN_FINAL,
  TMPFN_COUNT
};
static char s_tmpfns[TMPFN_COUNT][TMPFN_LEN] = {
  "",
  "",
  "",
  "",
  "",
};

static char *prv_check_tempfile(int idx) {
  char *str = s_tmpfns[idx];
  if (str[0] != '\0') {
    return str;
  }
  strcpy(str, "XXXXXX");
  mktemp(str);
  strcat(str, ".tmp");
  for (int i = 0; i < strlen(str); i++) {
    str[i] = toupper(str[i]);
  }
  return str;
}
static char *prv_cpp_file(void) {
  return prv_check_tempfile(TMPFN_CPP);
}
static char *prv_cf_parse_file(void) {
  return prv_check_tempfile(TMPFN_CF);
}
static char *prv_asm_file(void) {
  return prv_check_tempfile(TMPFN_ASM);
}
static char *prv_mixed_file(void) {
  return prv_check_tempfile(TMPFN_MIXED);
}
static char *prv_final_file(void) {
  return prv_check_tempfile(TMPFN_FINAL);
}

static int prv_execute_cmd(const char *cmd) {
  int ret;
  if (g_dryrun || (g_verbosity >= 1)) {
    printf("%s\n", cmd);
    ret = 0;
  }
  if (!g_dryrun) {
    ret = system(cmd);
  }
  if (g_verbosity >= 2) {
    printf("= %d (%#x)\n", ret, ret);
  }
  switch (ret) {
    case 0:
    case 0x200:
      return 1;
    case 0x100:
      return 0;
  }
}

static void prv_basedir_cmd(char **cmd, const char *basedir, const char *lsicmd) {
  if (basedir[0] != '\0') {
    kuso_catsprintf(cmd, "%s/", basedir);
  }
  kuso_catsprintf(cmd, "%s", lsicmd);
}
static void prv_lsic86_cmd(char **cmd, const char *lsicmd) {
  if(g_use_dosexec) {
    kuso_catsprintf(cmd, "dosexec ");
  }
  prv_basedir_cmd(cmd, g_lsic86, lsicmd);
}

static int run_cpp(void) {
  char *cmd = NULL;
  prv_lsic86_cmd(&cmd, "cpp");

  // added flags!
  kuso_catsprintf(&cmd, " -B");

  const char **incl_iter = kuso_pvec_iter(g_cpp.include_dirs, const char*);
  for (; *incl_iter != NULL; incl_iter++) {
    kuso_catsprintf(&cmd, " -I%s", *incl_iter);
  }

  const char **def_iter = kuso_pvec_iter(g_cpp.defines, const char*);
  for (; *def_iter != NULL; def_iter++) {
    kuso_catsprintf(&cmd, " -D%s", *def_iter);
  }

  // output file
  kuso_catsprintf(&cmd, " -o %s", prv_cpp_file());
  // input file
  kuso_catsprintf(&cmd, " %s", g_in_file);

  return prv_execute_cmd(cmd);
}

// cf -cs -chct 1.$$$ 2.$$$ 3.$$$
static int run_cf(void) {
  char *cmd = NULL;
  prv_lsic86_cmd(&cmd, "cf");

  // added flags!
  // signed char
  kuso_catsprintf(&cmd, " -cs");
  // dunno what these do
  kuso_catsprintf(&cmd, " -chct");

  // files
  kuso_catsprintf(&cmd, " %s %s %s", prv_cpp_file(), prv_cf_parse_file(), prv_asm_file());

  return prv_execute_cmd(cmd);
}

// cg86 -g 2.$$$ 3.$$$
static int run_cg86(void) {
  char *cmd = NULL;
  prv_lsic86_cmd(&cmd, "cg86");

  // added flags!
  // dunno what these do
  kuso_catsprintf(&cmd, " -g");

  // files
  kuso_catsprintf(&cmd, " %s %s", prv_cf_parse_file(), prv_asm_file());

  return prv_execute_cmd(cmd);
}

// merc -o 4.$$$ 3.$$$
static int run_merc(void) {
  char *cmd = NULL;
  prv_lsic86_cmd(&cmd, "merc");

  // output file
  kuso_catsprintf(&cmd, " -o %s", prv_mixed_file());

  // input file
  kuso_catsprintf(&cmd, " %s", prv_asm_file());

  return prv_execute_cmd(cmd);
}

static int run_nc86opt(void) {
  char *cmd = NULL;
  prv_basedir_cmd(&cmd, g_nc86opt, "nc86opt");

  // output file
  kuso_catsprintf(&cmd, " %s", prv_final_file());

  // input file
  kuso_catsprintf(&cmd, " %s", prv_mixed_file());

  return prv_execute_cmd(cmd);
}

// r86 -m in.c -o out.obj 4.$$$
static int run_r86(void) {
  char *cmd = NULL;
  prv_lsic86_cmd(&cmd, "r86");

  // source
  kuso_catsprintf(&cmd, " -m %s", g_in_file);

  // output file
  kuso_catsprintf(&cmd, " -o %s", g_out_file);

  // input file
  kuso_catsprintf(&cmd, " %s", prv_final_file());

  return prv_execute_cmd(cmd);
}

static void prv_cleanup_tmpfiles(void) {
  if (g_verbosity < 3) {
    for (int i = 0; i < TMPFN_COUNT; i++) {
      remove(s_tmpfns[i]);
    }
  }
}

void usage(void) {
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "\t%s arguments\n", g_app);
}

int main(int argc, char *argv[]) {
  g_app = argv[0];

  g_cpp.include_dirs = kuso_pvec_new(0);
  g_cpp.defines = kuso_pvec_new(0);

  // Number of arguments 'eaten'
  int move;
  for (int i = 1; i < argc; i += move) {
    move = prv_process_argument(argv + i);
    if (move == 0) {
      goto _fail;
    }
  }
  if (g_in_file == NULL) {
    fprintf(stderr, "Error: No input file\n");
    goto _fail;
  }
  if (g_out_file == NULL) {
    fprintf(stderr, "Error: No output file\n");
    goto _fail;
  }
  if (!run_cpp()) {
    fprintf(stderr, "CPP failed\n");
    goto _fail;
  }
  if (!run_cf()) {
    fprintf(stderr, "CF failed\n");
    goto _fail;
  }
  if (!run_cg86()) {
    fprintf(stderr, "CG86 failed\n");
    goto _fail;
  }
  if (!run_merc()) {
    fprintf(stderr, "MERC failed\n");
    goto _fail;
  }
  // run trp optimizer
  if (!run_nc86opt()) {
    fprintf(stderr, "NC86OPT failed\n");
    goto _fail;
  }
  if (!run_r86()) {
    fprintf(stderr, "R86 failed\n");
    goto _fail;
  }
  prv_cleanup_tmpfiles();
  return EXIT_SUCCESS;
_fail:
  prv_cleanup_tmpfiles();
  usage();
  return EXIT_FAILURE;
}
